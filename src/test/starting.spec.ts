import assert from "assert"; 
import chai from "chai"; 

const should = chai.should();

describe("Basic mocha test", function() {
    it("should deal with objets", function() {
        const obj = { name: 'Nahuel', gender: 'male' };
        const obj2 = { name: 'Nahuel', gender: 'male' }; 
        
        obj2.should.be.deep.equal(obj);
    })

    it("Should allow you test null values", function() {
        const isNull = null; 
        should.not.exist(isNull);
    })
});
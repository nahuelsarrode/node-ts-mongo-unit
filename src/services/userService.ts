import logger from "../common/logger"; 
import User ,{IUser} from "../models/user.model";

export const getByEmail = async (params: string) => {
    try {
        const user = await User.find({ email: params });
        
        if (!user || !user.length) return null;

        return user[0];   
    } catch (error) {
        logger.error("Cant execute the query", error);
        throw error; 
    }
}
// In this case, when you only have a simple statement of code dont need to implmentate try and catch. 
export const create = (user: IUser) => {
    return user.save(); 
}

export const getAll = async () => {
    try {
        const users = User.find();

        if (!users) return null; 
        
        return users;  
    } catch(error) {
        logger.error("Cant execute the query", error);
        throw error;
    }
}

export const update = async (user: IUser) => {
    try {
        const { id } = user;
        await User.findByIdAndUpdate(id,user);
    } catch (error) {
        logger.error("Cant execute query", error);
        throw error; 
    } 
}

// In this case, when you only have a simple statement of code dont need to implmentate try and catch. 
export const remove = (email: string) => {
    return User.findOneAndRemove({email});
}


import assert from "assert"; 
import {expect} from "chai";
import sinon from "sinon";

import { isAuthorized, isAuthorizedAsync, setRoles, deleteRoles, getIndex} from "../../controllers/auth.controller";
describe("AuthController", function() {
    beforeEach('Before each setting up', function settingUpRoles() {
        setRoles(['user']);
    });  

    afterEach(function() {
        deleteRoles();
    })

    describe("isAuthorized", function() {
        it("Should return false if not authorized", function() {
            const isAuth = isAuthorized('admin'); 
            expect(isAuth).to.be.false;
        })

        it("Should return true if is authorized", function() {
            setRoles(['admin']);
            const isAuth = isAuthorized('admin'); 
            expect(isAuth).to.be.true; 
        })
    })

    describe("isAuthorizedAsync", function() {
        it("Should return false if not authorizedAsync", function(done) {
            this.timeout(2500); 
            isAuthorizedAsync('admin', function(isAuth: Boolean) {
                expect(isAuth).to.be.false;
                done();
            });
        })

        it("Should return true if is authorizedAsync", function(done) {
            this.timeout(2500); 
            setRoles(['admin']);
            isAuthorizedAsync('admin', function(isAuth: Boolean){
                expect(isAuth).to.be.true;
                done();
            });
        })

        it("Should render index", function() {
            const req = {}; 
            const res = {
                render: sinon.spy()
            }
            getIndex(req, res);
            res.render.calledOnce.should.be.true;
            res.render.firstCall.args[0].should.be.equal('index');
        })
    })
})  
